
import json
import numpy as np

import neomodel as neo

import config
import torch
from bert import bert
from model import model


# Слова для сплиттеров
SENTENCE_SPLITTERS = '.?!'
WORD_SPLITTERS = list("""~`@#$%^&*()_-+=}{][;:'"\\|<>""") + ["--"]


def byid(cls, nid: str):
    results, _ = neo.db.cypher_query(f"MATCH (b:{cls.__name__}) where id(b) = {nid} return b;", {})
    return cls.inflate(results[0][0]) if results else None

# Добавляем метод, не добавляя новый класс
neo.StructuredNode.byid = classmethod(byid)


class TensorProperty(neo.properties.NormalizedProperty):
    def __init__(self, *args, **kwargs):
        super(TensorProperty, self).__init__(*args, **kwargs)

    @neo.properties.validator
    def inflate(self, value):
        return np.array(json.loads(value))

    @neo.properties.validator
    def deflate(self, value):
        return self.normalize(value)

    def normalize(self, value):
        if isinstance(value, torch.Tensor):
            value = value.cpu().numpy()

        if isinstance(value, np.ndarray):
            value = value.tolist()

        return json.dumps(value)


class Word(neo.StructuredNode):
    sentence = neo.RelationshipFrom('Sentence', 'WORD_OF')

    text = neo.StringProperty(required=True)
    start = neo.IntegerProperty(required=True)
    end = neo.IntegerProperty(required=True)

    _relations = neo.Relationship('Word', 'CONNECTED')

    @property
    def positions(self):
        return self.start, self.end

    @property
    def relations(self):
        return list(self._relations)[::-1]

    @property
    def json(self):
        r = {
            'text': self.text,
            'relations': [x.id for x in self.relations]
        }
        return json.dumps(r)


class Sentence(neo.StructuredNode):
    article = neo.RelationshipFrom('Article', "SENTENCE_OF")

    text = neo.StringProperty(required=True)
    _words = neo.RelationshipTo('Word', 'WORD_OF')
    #  После того как будет обработано, заполняются токены и фичи
    text_tokens = neo.ArrayProperty(neo.StringProperty())
    text_features = TensorProperty()

    @property
    def words(self):
        return list(self._words)[::-1]

    @staticmethod
    def Create(text: str):
        self = Sentence(text=text).save()

        for w in self.split_words():
            self._words.connect(Word(**w).save())
        return self

    def split_words(self):
        sentence = str(self.text)
        text_tokens = bert.tokenizer.tokenize(sentence)
        tokens = bert.tokenizer.convert_tokens_to_ids(text_tokens)
        self.text_tokens = text_tokens
        self.text_features = bert.get_feature(tokens)
        self.save()
        for r in WORD_SPLITTERS:
            sentence = sentence.replace(r, ' ')
        words = [{'text': x, 'start': 0, 'end': 0} for x in sentence.split() if x]
        i = 0
        try:
            for w in words:
                while w['text'][0] not in self.text_tokens[i]:
                    i += 1
                w['start'] = i
                for l in w['text']:
                    while l not in self.text_tokens[i]:
                        i += 1
                w['end'] = i+1
                i += 1
        except IndexError:
            pass
        return words

    def process(self):
        relations = model.process(self)
        for x, y in relations:
            x._relations.connect(y)
        return self

    def __iter__(self):
        return self.words.__iter__()


class Article(neo.StructuredNode):
    text = neo.StringProperty(required=True, index=True)
    _sentences = neo.RelationshipTo('Sentence', 'SENTENCE_OF')
    processed = neo.BooleanProperty(default=False)

    @property
    def sentences(self):
        return list(self._sentences)[::-1]

    @property
    def words(self):
        return sum([x.words for x in self.sentences], [])

    @staticmethod
    def Create(text: str, force: bool = False):
        if not force:
            self = Article.nodes.first_or_none(text=text)
            if self:
                return self

        self = Article(text=text).save()

        for s in self.split_sentences():
            self._sentences.connect(Sentence.Create(**s).save())
        return self

    def process(self):
        if config.readonly:
            print("READONLY MODE!")
            return self
        for s in self.sentences:
            s.process()
        self.processed = True
        self.save()
        return self

    def split_sentences(self):
        sent = str(self.text).strip('\n ').replace('\n', ' ').replace('  ', ' ')
        for r in SENTENCE_SPLITTERS:
            sent = sent.replace(r, '. ')
        for s in sent.split('. '):
            if not s:
                continue
            print(s)
            yield {'text': s.strip()}

    def __iter__(self):
        return self.sentences.__iter__()

    @property
    def json(self):
        words = sum([x.words for x in self.sentences], [])
        d = {f'word{w.id}': {
            'text': w.text,
            'relations': [f'word{x.id}' for x in w.relations]
        } for w in words}
        return json.dumps(d)