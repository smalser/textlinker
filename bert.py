import os
from typing import List

import numpy as np
import torch
from torch.nn.utils.rnn import pad_sequence
from transformers import BertTokenizer, BertModel

import config


class Bert:
    device = 'cpu'
    MODEL_DIR = "BERT/rubert"  # path to rubert model
    VOCAB_DIR = MODEL_DIR + "/vocab.txt"  # rubert vocab
    FEATURE_LENGTH = 768
    def __init__(self, *args, **kwargs):
        for k,i in kwargs.items():
            if i != None:
                self.__dict__[k] = i
        print('Initing bert model from:', os.path.abspath(self.MODEL_DIR))
        self.model = BertModel.from_pretrained(self.MODEL_DIR)
        self.model.eval()
        self.tokenizer = BertTokenizer(self.VOCAB_DIR, do_basic_tokenize=False)
        self.pad_id = self.tokenizer.pad_token_id
        self.to(self.device)

    def to(self, device: str):
        self.device = device
        print('Moving bert tensors to:', self.device)
        self.model.to(device)
    
    def get_feature(self, tokens: str) -> np.array:
        #input_ids = pad_sequence(torch.Tensor(tokens).to(self.device), padding_value=self.pad_id, batch_first=True)
        input_ids = torch.Tensor(tokens).to(self.device)
        tokens_tensor = torch.LongTensor([[int(id) for id in input_ids]]).to(self.device)
        segments_tensors = torch.LongTensor([[0] * len(tokens_tensor)]).to(self.device)

        with torch.no_grad():
            outputs = self.model(tokens_tensor, token_type_ids=segments_tensors)
        return outputs[0][0].cpu().numpy()

bert = Bert() if not config.readonly else None