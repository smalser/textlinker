import json

from flask_paginate import Pagination
from flask import render_template, send_from_directory, redirect, request
from server import app
from result_builder import generate
from models import Article


# Для работы тимплейтовских штук
@app.route('/assets/<path:path>')
@app.route('/result/assets/<path:path>')
def send_js(path):
    return send_from_directory('templates/assets/', path)


# Хэндлер 404
@app.errorhandler(404)
def error404(e):
    return render_template('404.html')

# Хэндлер 500
@app.errorhandler(500)
def error500(e):
    return render_template('500.html')


#  Главная страница
@app.route('/', redirect_to='/index')
@app.route('/index.html', redirect_to='/index')
@app.route('/index')
def index_page():
    return render_template('index.html')


#  Страница ввода
@app.route('/input.html', redirect_to='/input')
@app.route('/input')
def input_page():
    return render_template('input.html')


# Страница вывода резлуьтата
@app.route('/result/<string:id>')
def result_page(id: str):
    article = Article.byid(id)
    print(article)
    article, words = generate(article)
    return render_template('result.html', article=article, words=words)


# Сюда приходит запрос на генерацию
@app.route('/process_text', methods=["POST"])
def process_page():
    data = request.form.to_dict()
    print(data)
    text = data.get('text', None)
    if text is None:
        raise Exception(500)
    id = Article.Create(text).process().id
    return redirect(f'/result/{id}')


# Страница поиска
@app.route('/search.html', redirect_to='/search')
@app.route('/search')
def search_page():
    per_page = 10
    args = request.args
    print(args)

    #filtering
    articles = Article.nodes.filter()

    # pagination
    page = args.get('page', type=int, default=1)
    selected_articles = articles[(page-1)*per_page:page*per_page]

    pagination = Pagination(page=page, per_page=per_page, total=len(articles), css_framework='bootstrap4')
    return render_template('search.html', page=page, articles=selected_articles, pagination=pagination)


# Страница с информацией
@app.route('/info.html', redirect_to='/info')
@app.route('/info')
def info_page():
    return render_template('info.html')
