import numpy as np

import torch
from torch import nn

import config


class LinkModel(nn.Module):
    def __init__(self, filename: str,
                 input_size: int, feature_size: int, hidden_size: int,
                 layers: int = 1, target_size: int = 2,
                 device: str = 'cpu'):
        super(LinkModel, self).__init__()
        self.device = device
        self.input_size = input_size
        self.lstm = nn.LSTM(input_size+feature_size, hidden_size, num_layers=layers, bidirectional=True).to(self.device)
        self.linear = nn.Linear(2*hidden_size, target_size).to(self.device)
        self.load(filename)

    def forward(self, x):
        size = len(x)
        input_tensor = torch.Tensor(x).view(size, 1, -1).to(self.device)
        lstm_out, _ = self.lstm(input_tensor)
        linear = self.linear(lstm_out.view(size, -1))
        scores = nn.functional.log_softmax(linear, dim=1).to(self.device)
        return scores

    def load(self, filename: str):
        print("Loading model from: ", filename)
        self.load_state_dict(torch.load(filename, map_location=self.device))

    def to(self, device):
        self.device = device
        self.lstm = self.lstm.to(self.device)
        self.linear = self.linear.to(self.device)

    def process(self, sentence):
        words = sentence.words
        relations = set()
        pos2word = {x.positions: x for x in words}
        for w in sentence.words:
            pos_vec = [(1 if w.positions[0] <= i < w.positions[1] else 0) for i in range(len(sentence.text_features))]
            input_vec = np.hstack((sentence.text_features, np.array([pos_vec]).T))
            with torch.no_grad():
                out = self(input_vec).data.max(1)[1].cpu().numpy()
            for x, y in pos2word:
                if out[x:y].all():
                    #  if (w.positions, (x,y)) not in relations:
                    relations.add((w.positions, (x, y)))
        return [(pos2word[x], pos2word[y]) for x, y in relations]


model = LinkModel(**config.MODEL) if not config.readonly else None
