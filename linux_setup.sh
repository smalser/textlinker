#! /bin/sh
sudo apt update
sudo apt install -y python3-pip
sudo apt install -y default-jdk
sudo apt install -y python3-venv
python3 -m venv .env
source .env/bin/activate
pip install -f https://download.pytorch.org/whl/torch_stable.html -r requirements.txt

wget --output-document=neo4j-community-3.5.17-unix.tar.gz https://neo4j.com/artifact.php?name=neo4j-community-3.5.17-unix.tar.gz
tar xvzf neo4j-community-3.5.17-unix.tar.gz
rm neo4j-community-3.5.17-unix.tar.gz
sed -i 's/#dbms.connectors.default_listen_address=0.0.0.0/dbms.connectors.default_listen_address=0.0.0.0/' ~/neo4j-community-3.5.17/conf/neo4j.conf
neo4j-community-3.5.17/bin/neo4j start
