import os
from typing import List

import numpy as np

import torch
from torch.nn.utils.rnn import pad_sequence
from transformers import BertTokenizer, BertModel

class Sentence:
    REPLACES = list(".,?!@#$%^&*()/\"':;-") + ['--']
    def __init__(self, text: str):
        self.text = text
        self.words = []
        self.positions = []
        self.text_tokens = []
        self.features = np.array([])
        self.vectors = []
        
    def get_positions(self, bert):
        self.text_tokens = bert.tokenizer.tokenize(self.text)
        positions = self.get_words_positions()
        self.words = [x['word'] for x in positions]
        self.positions = [(x['start'], x['end']) for x in positions]
        
    @staticmethod
    def New(text, bert):
        self = Sentence(text)
        self.get_positions(bert)
        return self
    
    @staticmethod
    def Iter(*sentences):
        for s in sentences:
            for r in s:
                yield r
    
    def __iter__(self):
        for v in self.vectors:
            yield np.hstack((self.features, np.array([v['input']]).T)), v['output']    
    
    def mark(self, *words):
        vec = [0 for _ in range(len(self))]
        for w in words:
            vec[w[0]:w[1]] = [1 for _ in range(w[1]-w[0])]
        return vec
    
    def unmark(self, input, output):
        if isinstance(output, torch.Tensor):
            output = output.cpu().numpy()
        else:
            output = np.array(output)
        ret = []
        for i, (x,y) in enumerate(self.positions):
          if input[x:y].all():
              input_word = self.words[i]
          if output[x:y].all():
              ret.append(self.words[i])
        print(input_word, ret)
        
    def get_words_positions(self):
        sentence = self.text
        for r in self.REPLACES:
            sentence = sentence.replace(r, ' ')
        words = [{'word':x, 'start':0, 'end': 0} for x in sentence.split() if x]
        i = 0
        try:
            for w in words:
                while (w['word'][0] not in self.text_tokens[i]):
                    i += 1
                w['start'] = i
                for l in w['word']:
                    while (not l in self.text_tokens[i]):
                        i += 1
                w['end'] = i+1
                i += 1
        except IndexError:
            pass
        return words
    
    def __len__(self):
        return len(self.text_tokens)
    
    def __str__(self):
        return f'Sentence("{self.text}")'
    
    def __repr__(self):
        return str(self)


class Bert:
    device = 'cpu'
    MODEL_DIR = "BERT/rubert"  # path to rubert model
    VOCAB_DIR = MODEL_DIR + "/vocab.txt"  # rubert vocab
    FEATURE_LENGTH = 768
    def __init__(self, *args, **kwargs):
        for k,i in kwargs.items():
            if i != None:
                self.__dict__[k] = i
        print('Initing bert model from:', os.path.abspath(self.MODEL_DIR))
        self.model = BertModel.from_pretrained(self.MODEL_DIR)
        self.model.eval()
        self.tokenizer = BertTokenizer(self.VOCAB_DIR, do_basic_tokenize=False)
        self.pad_id = self.tokenizer.pad_token_id
        print('Moving tensors to:', self.device)
        self.model.to(torch.device(self.device)) # moving to gpu if it contains
    
    def process_text(self, text: str):
        #corpus = [prep.basic_preprocessing(text) for text in corpus]
        sentences = [Sentence.New(text, self)]
        tokens = [torch.tensor(self.tokenizer.convert_tokens_to_ids(sent.text_tokens)) for sent in sentences]
        input_ids = pad_sequence(tokens, padding_value=self.pad_id, batch_first=True)
        features = self.get_feature(input_ids)
        for sent, feat in zip(sentences, features):
            sent.features = feat
        return sentences
    
    def get_feature(self, input_ids: List[str]) -> np.array:
        features = []
        for i, input_token_ids in enumerate(input_ids):
            #print(i, "/", len(input_ids))
            
            tokens_tensor = torch.LongTensor([[int(id) for id in input_token_ids]]).to(self.device)
            segments_tensors = torch.LongTensor([[0] * len(input_ids)]).to(self.device)

            with torch.no_grad():
                outputs = self.model(tokens_tensor, token_type_ids=segments_tensors)
                features.append(outputs[0][0].numpy())

        return np.array(features)
        
#bert = Bert()

if __name__ == "__main__":
    m = Bert()
    r = m.process_text("Такие дела? Ну и ну...")

    z = m.tokenizer.tokenize('Что за дела, Мда')