import os
from conllu import parse_incr, parse
import numpy as np


import os
from typing import List

import numpy as np
import torch
from torch.nn.utils.rnn import pad_sequence
from transformers import BertTokenizer, BertModel

class Bert:
    device = 'cpu'
    MODEL_DIR = "BERT/rubert"  # path to rubert model
    VOCAB_DIR = MODEL_DIR + "/vocab.txt"  # rubert vocab
    FEATURE_LENGTH = 768
    def __init__(self, *args, **kwargs):
        for k,i in kwargs.items():
            if i != None:
                self.__dict__[k] = i
        print('Initing bert model from:', os.path.abspath(self.MODEL_DIR))
        self.model = BertModel.from_pretrained(self.MODEL_DIR)
        self.model.eval()
        self.tokenizer = BertTokenizer(self.VOCAB_DIR, do_basic_tokenize=False)
        self.pad_id = self.tokenizer.pad_token_id
        self.to(self.device)

    def to(self, device: str):
        self.device = device
        print('Moving bert tensors to:', self.device)
        self.model.to(device)
    
    def get_feature(self, tokens: str) -> np.array:
        #input_ids = pad_sequence(torch.Tensor(tokens).to(self.device), padding_value=self.pad_id, batch_first=True)
        input_ids = torch.Tensor(tokens).to(self.device)
        tokens_tensor = torch.LongTensor([[int(id) for id in input_ids]]).to(self.device)
        segments_tensors = torch.LongTensor([[0] * len(tokens_tensor)]).to(self.device)

        with torch.no_grad():
            outputs = self.model(tokens_tensor, token_type_ids=segments_tensors)
        return outputs[0][0].cpu().numpy()


bert = Bert(MODEL_DIR='../BERT/rubert', VOCAB_DIR='../BERT/rubert/vocab.txt')

import pickle

class Sentence:
    REPLACES = list(".,?!@#$%^&*()/\"':;-") + ['--']
    def __init__(self, text: str):
        self.text = text
        self.words = []
        self.positions = []
        self.text_tokens = []
        self.features = np.array([])
        self.vectors = []
        
    def get_positions(self, bert):
        self.text_tokens = bert.tokenizer.tokenize(self.text)
        positions = self.get_words_positions()
        self.words = [x['word'] for x in positions]
        self.positions = [(x['start'], x['end']) for x in positions]
        
    @staticmethod
    def New(text, bert):
        self = Sentence(text)
        self.get_positions(bert)
        return self
    
    @staticmethod
    def Iter(*sentences):
        for s in sentences:
            for r in s:
                yield r
    
    def __iter__(self):
        for v in self.vectors:
            yield np.hstack((self.features, np.array([v['input']]).T)), v['output']    
    
    def mark(self, *words):
        vec = [0 for _ in range(len(self))]
        for w in words:
            vec[w[0]:w[1]] = [1 for _ in range(w[1]-w[0])]
        return vec
    
    def unmark(self, input, output):
        if isinstance(output, torch.Tensor):
            output = output.cpu().numpy()
        else:
            output = np.array(output)
        ret = []
        for i, (x,y) in enumerate(self.positions):
          if input[x:y].all():
              input_word = self.words[i]
          if output[x:y].all():
              ret.append(self.words[i])
        print(input_word, ret)
        
    def get_words_positions(self):
        sentence = self.text
        for r in self.REPLACES:
            sentence = sentence.replace(r, ' ')
        words = [{'word':x, 'start':0, 'end': 0} for x in sentence.split() if x]
        i = 0
        try:
            for w in words:
                while (w['word'][0] not in self.text_tokens[i]):
                    i += 1
                w['start'] = i
                for l in w['word']:
                    while (not l in self.text_tokens[i]):
                        i += 1
                w['end'] = i+1
                i += 1
        except IndexError:
            pass
        return words
    
    def __len__(self):
        return len(self.text_tokens)
    
    def __str__(self):
        return f'Sentence("{self.text}")'
    
    def __repr__(self):
        return str(self)


def load_conllu(filename="CONLLU/ru_gsd-ud-train.conllu"):
    data_file = parse_incr(open(filename, "r", encoding="utf-8"))
    return list(parse_incr(data_file))


classes = []
def dataset(filename: str):
    data_file = parse_incr(open(filename, "r", encoding="utf-8"))
    for i, root in enumerate(data_file):
        if (i%100 == 0):
            print(i)
        text = root.metadata['text']
        sentence = Sentence(text)
        sentence.words = [w['form']  for w in root]
        
        tokens_pos = [bert.tokenizer.convert_tokens_to_ids(bert.tokenizer.tokenize(x)) for x in sentence.words]
        sentence.text_tokens = sum(tokens_pos, [])
        sentence.features = bert.get_feature(sentence.text_tokens)
        # Set positions
        x = 0
        sentence.positions = []
        for w in tokens_pos:
            y = x+len(w)
            sentence.positions.append((x,y))
            x = y
        
        dependences = {}
        valid_dep = ('nmod')
        for word in root:
            if word['deprel'] in valid_dep and word['head'] != 0:
                dependences.setdefault(word['head']-1, []).append(word)
                        
        data = []
        for key, value in dependences.items():
            # print(root[key]['form'], [x['form'] for x in value])
            input_vec = sentence.mark(sentence.positions[key-1])
            output_vec = sentence.mark(*[sentence.positions[x['id']-1] for x in value])
            data.append({'input':input_vec, 'output':output_vec})

        sentence.vectors = data
        yield sentence

train = list(dataset("../CONLLU/ru_gsd-ud-train.conllu"))
pickle.dump(train, open('../str_train.pickle', 'wb'),)
test = list(dataset("../CONLLU/ru_gsd-ud-test.conllu"))
pickle.dump(test, open('../str_test.pickle', 'wb'))

train = pickle.load(open('../str_train.pickle', 'rb'),)

raise Exception()

def file_iter(folder: str = "D:/Diplom/CONLLU/tagged_texts", start: int = 0, limit: int = None):
    i = 0
    for file in os.listdir(folder.replace('\\', '/')):
        cur_data = parse(open(folder+'/'+file, 'r', encoding='utf-8').read())
        for x in cur_data:
            if i < start:
                continue
            if i is not None and i == limit:
                return
            print(i, '#########')
            print(x.metadata['text'])
            try:
                yield TokenList2MySet(x)
            except KeyError:
                with open('errors.txt', 'a', encoding='utf-8') as f:
                    f.write(f'{file}: {x.metadata["text"]}\n')
            i += 1

def create_links(collu, sent):
    cwords = collu
    links = {}
    
    i = 0
    for w in cwords:
        #  Пропускаем знаки
        if w['form'] in Sentence.REPLACES:
            continue
        try:
            print(w['form'], sent.words[i], sent.positions[i])
        except:
            print(w['form'])
            print(i)
            print(sent.words)
            raise Exception()
        links[w['id']-1] = sent.positions[i]
        i += 1
        if i == len(sent.words):
            break
    return links

def TokenList2MySet(root):
    text = root.metadata['text']
    processed = bert.process_text(text)[0]
    valid_dep = ('nmod')
    dependences = {}
    for word in root:
        if word['deprel'] in valid_dep and word['head'] != 0:
            dependences.setdefault(word['head']-1, []).append(word)
    
    links = create_links(root, processed)
    data = []
    for key, value in dependences.items():
        print(root[key]['form'], [x['form'] for x in value])
        input_vec = processed.mark(links[key])
        output_vec = processed.mark(*[links[x['id']-1] for x in value])
        data.append({'input':input_vec, 'output':output_vec})
    
    processed.vectors = data
    return processed


    
'''
ret = list(file_iter(start=0))
pickle.dump(data, open('bigguy.pickle', 'wb'))

import bert
import pickle
bert.bert = None
data = pickle.load(open('bigguy.pickle', 'rb'))[:50000]
import random
random.shuffle(data)

l = len(data)
a = data[:40000]
pickle.dump(a, open('train.pickle', 'wb'))
del a
b = data[40000:]
del data
pickle.dump(b, open('test.pickle', 'wb'))
del b

################

filename="CONLLU/ru_gsd-ud-train.conllu"
data_file = parse_incr(open(filename, "r", encoding="utf-8"))

t = data_file.__next__()
bert = Bert()
a = TokenList2MySet(t)
#a.vectors = [{'input':a.mark(x), 'output':[]} for x in a.positions]
for i, (x,y) in enumerate(a):
    unmark(a, x, y)
    
   ''' 
# Do a new dataset
bert = Bert()

filename="CONLLU/ru_gsd-ud-train.conllu"
data_file = parse_incr(open(filename, "r", encoding="utf-8"))
parsed = []
for x in data_file:
    try:
        z = TokenList2MySet(x)
    except KeyError:
        continue
    if z.vectors:
        parsed.append(z)
    
pickle.dump(parsed, open('ru_gsd_train.pickle', 'wb'))


filename="CONLLU/ru_gsd-ud-test.conllu"
data_file = parse_incr(open(filename, "r", encoding="utf-8"))
parsed = []
for x in data_file:
    try:
        z = TokenList2MySet(x)
    except KeyError:
        continue
    if z.vectors:
        parsed.append(z)
    
pickle.dump(parsed, open('ru_gsd_test.pickle', 'wb'))