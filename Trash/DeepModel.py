import torch
import sklearn
from sklearn.datasets import load_digits
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
from torchvision.datasets import MNIST
from torchvision import transforms
import PIL
import numpy as np


#img = PIL.Image.fromarray(np.squeeze(p))


class Net(nn.Module):
    def __init__(self, picsize, l1size, l2size, resultsize):
        super(Net,self).__init__()
        self.fc1 = nn.Linear(picsize, l1size)
        self.fc2 = nn.Linear(l1size, l2size)
        self.fc3 = nn.Linear(l2size, resultsize)


    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return F.log_softmax(x)


class Model1:
    def __init__(self, picsize, l1size, l2size, resultsize):
        self.picsize = picsize
        self.net = Net(picsize, l1size, l2size, resultsize)
        self.criterion = nn.NLLLoss()

    def LoadData(self, data, target, coef=0.8):
        self.classes = list(set(list(target)))
        self.train_set = np.array(data[:int(len(data)*coef)])
        self.test_set = np.array(data[int(len(data)*coef):])

        self.train_target = np.array(target[:int(len(target)*coef)])
        self.test_target = np.array(target[int(len(target)*coef):])

    def Train(self, batch_size=50, lr=0.01, momentum=0.9):
        self.batch_size = batch_size
        self.optimizer = torch.optim.SGD(self.net.parameters(), lr=lr, momentum=momentum)
        for epoch in range(5):
            #for batch_idx, (batch, batch_target) in enumerate(zip(self.train_set[::batch_size], self.train_target[::batch_size])):
            index = 0
            batch_idx = 0
            while batch_idx*batch_size < len(self.train_set):
                index = batch_size*batch_idx
                batch = self.train_set[index:index+batch_size]
                batch_target = self.train_target[index:index+batch_size]
                data = Variable(torch.Tensor(batch))
                #print(data)
                target = Variable(torch.LongTensor([self.classes.index(x) for x in batch_target]))
                data = data.view(-1, self.picsize)
                self.optimizer.zero_grad()
                net_out = self.net(data)
                #print(net_out, target)
                loss = self.criterion(net_out, target)
                loss.backward()
                self.optimizer.step()
                if batch_idx % 100 == 0:
                    print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                            epoch, batch_idx * len(data), len(self.train_set),
                                    100. * index / len(self.train_set), loss.item()))
                batch_idx += 1
        self.Metric()
    
    def Metric(self):
        test_loss = 0.0
        correct = 0

        batch_idx, index = 0, 0
        while batch_idx*self.batch_size < len(self.test_set):
            index = self.batch_size*batch_idx

            batch = self.test_set[index:index+self.batch_size]
            batch_target = self.test_target[index:index+self.batch_size]
            
            data = Variable(torch.Tensor(batch))
            target = Variable(torch.LongTensor([self.classes.index(x) for x in batch_target]))
            data = data.view(-1, self.picsize)
            
            self.optimizer.zero_grad()
            net_out = self.net(data)
            test_loss += self.criterion(net_out, target).item()
            pred = net_out.data.max(1)[1]
            correct += pred.eq(target.data).sum()
            batch_idx += 1

        test_loss /= len(self.test_target)
        print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
            test_loss, correct, len(self.test_target),
            100. * correct / len(self.test_target)))

    def Predict(self, data):
        d = Variable(torch.Tensor(data))
        d = d.view(-1, self.picsize)
        net_out = self.net(d)
        pred = net_out.data.max(1)[1]
        print(pred)
        return self.classes[pred]



trans = transforms.ToTensor()
train_loader = torch.utils.data.DataLoader(dataset=MNIST("./data", download=True, transform = trans, train=True),
                                batch_size=1,
                                shuffle=True
                                )
data, target = [], []
for x,y in train_loader:
    data.append(x[0].numpy())
    target.append(int(y[0]))

m = Model1(28*28, 200, 200, 10)
m.LoadData(data, target)
m.Train()
m.Predict(data[12])


m.Predict(1 - np.array(PIL.Image.open("1.jpg").convert("L"))/255)

"""
    trans = transforms.ToTensor()

    train_loader = torch.utils.data.DataLoader(dataset=MNIST("./data", download=True, transform = trans, train=True),
                                batch_size=10,
                                shuffle=True
                                )
    test_loader = torch.utils.data.DataLoader(dataset=MNIST("./data", download=True, transform = trans, train=False),
                                batch_size=10,
                                shuffle=False
                                )
"""
    


    

