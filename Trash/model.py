import os
import pickle
import numpy as np

import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
from sklearn.metrics import f1_score

from bert import Bert, Sentence
import bert as bertm


class Net(nn.Module):
    def __init__(self, input_size, feature_size, hidden_size, layers = 1, target_size = 2, device='cpu'):
        super(Net,self).__init__()
        self.device = device
        self.input_size = input_size
        self.lstm = nn.LSTM(input_size+feature_size, hidden_size, bidirectional=True).to(self.device)
        self.linear = nn.Linear(2*hidden_size, target_size).to(self.device)

    def forward(self, x):
        size = len(x)
        input_tensor = torch.Tensor(x).view(size, 1, -1).to(self.device)
        lstm_out, _ = self.lstm(input_tensor)
        linear = self.linear(lstm_out.view(size, -1))
        scores = F.log_softmax(linear, dim=1).to(self.device)
        return scores
    
    def to(self, device):
        self.device = device
        self.lstm = self.lstm.to(self.device)
        self.linear = self.linear.to(self.device)

class Model:
    input_size = 768
    feature_size = 1
    hidden_size = 768
    layers = 1
    target_size = 2
    device = 'cpu'
    loss = nn.NLLLoss
    
    def __init__(self, name='model', **kwargs):
        self.name = name
        for k,i in kwargs.items():
            if i != None:
                self.__dict__[k] = i
        self.model = Net(self.input_size, self.feature_size,
                         self.hidden_size, self.layers, self.target_size,
                         device=self.device)
        self.loss_function = self.loss()

    def set_data(self, train_set, test_set, classes):
        self.calsses = classes
        self.train_set = train_set
        self.test_set = test_set
        return self
                
    def savestate(self, path='', name=''):
        name = name or self.name
        fl = f'{path}/{name}/model{self.epoch}.model'
        try:
            os.mkdir(self.name)
        except:
            pass
        score = self.F1()
        with open(f'{name}/stat.txt', 'a', encoding='utf-8') as f:
            f.write(f'{self.epoch}\t{score}\n')
        self.save(fl)
        
    def save(self, filename):
        print("Saving model to: ", filename)
        torch.save(self.model.state_dict(), filename)
    
    def load(self, filename):
        print("Saving model from: ", filename)
        self.model.load_state_dict(torch.load(filename, map_location=self.device))
        
    def eval(self):
        self.model.eval()

    def train(self, epochs=10, lr=0.01, momentum=0, weight_decay=0):
        print(f'Start training with [epochs={epochs}, lr={lr}, momentum={momentum}, weight_decay={weight_decay}]')
        for self.epoch in range(epochs):
            optimizer = torch.optim.SGD(self.model.parameters(), lr=lr, momentum=momentum)
            ln = '?'
            for i, (x, y) in enumerate(Sentence.Iter(*self.train_set)):
                # Step 1. Remember that Pytorch accumulates gradients.
                self.model.zero_grad()

                # Step 3. Run our forward pass.
                tag_scores = self.model(x)

                # Step 4. Compute the loss, gradients, and update the parameters by
                loss = self.loss_function(tag_scores, torch.LongTensor(y).to(self.device))
                loss.backward()
                optimizer.step()
                if (i%5000 == 0):
                    print('Train Epoch: {} [{}/{}]\tLoss: {:.6f}'.format(
                        self.epoch, i, ln, loss.item()))
            ln = i
            self.savestate()
            lr*= 0.85
    
    def F1(self, corpus=None):
        corpus = corpus or self.test_set
        scores = []
        with torch.no_grad():    
            for x, y in Sentence.Iter(*corpus):
                tag_scores = self.model(x)
                scores.append(f1_score(tag_scores.data.max(1)[1].cpu(), y))
        score = np.mean(scores)
        print('F1(mean):', score)
        return score

    def predict(self, text=None, data=None):
        if (text is None and data is None):
            print("Empty data")
            return
        if data is None:
            data = bert.process_text(text)[0]
        data.vectors = [{'input':data.mark(x), 'output':[]} for x in data.positions]
        for x, _ in data:
            out = self.model(x).data.max(1)[1].cpu().numpy()
            data.unmark(x, out)

"""

train = pickle.load(open('ru_gsd_train.pickle', 'rb'))
test = pickle.load(open('ru_gsd_test.pickle', 'rb'))

#x, y = corpus1[0].__iter__().__next__()
# Работа с моделью
model = Model('model2').set_data(train, test)
model.train()


bert = Bert()
model.load_state_dict(torch.load('d:/GDrive/Diplom/test1/state2.model', map_location='cpu'))
model.eval()
a = bert.process_text('Пошел нахуй, ебучий блядский Суздаль, ебали твой гадский рот')[0]
a.vectors = [{'input':a.mark(x), 'output':[]} for x in a.positions]
for i, (x,y) in enumerate(a):
    #unmark(a, i, x)
    out = model(x).data.max(1)[1].cpu().numpy()
    #out == y
    unmark(a, x, out)

"""