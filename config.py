import neomodel as neo
neo.config.ENCRYPTED_CONNECTION = False
neo.config.DATABASE_URL = 'bolt://neo4j:neo4j@localhost'

debug = False
readonly = debug
DEVICE = 'cpu'

MODEL = {
    'filename': 'bestmodel.model',
    'input_size': 768,
    'feature_size': 1,
    'hidden_size': 768,
    'layers': 1,
    'target_size': 2,
    'device': DEVICE,
}