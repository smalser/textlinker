import config
from flask import Flask


class Server(Flask):
    debug = config.debug

    def __init__(self, ip: str = '0.0.0.0', port: int = 5000):
        super(Server, self).__init__(__name__)
        self.ip = ip
        self.port = port

    def run(self):
        print(f'Starting server on {self.ip}:{self.port}')
        super().run(host=self.ip, port=self.port, debug=self.debug)


app = Server(port=8000)

