from models import Article, Word
from string import Template


wordstart = """<span id="word{id}" onclick="clicked(this)" onmouseout="unhover(this)" onmouseover="hovered(this)" display="block">{text}</span>"""
class WebWord:
    def __init__(self, word: Word):
        self.id = word.id
        self.word = word
        self.text = word.text

    def __repr__(self):
        return str(self)

    def __str__(self):
        txt = wordstart.format(**self.__dict__)
        return txt


def generate(article):
    words = [WebWord(x) for x in article.words]
    res = " ".join([str(x) for x in words])
    print(res)
    #scr = script.substitute(words=article.json)

    return res, article.json
