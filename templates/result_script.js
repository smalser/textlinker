// Loading words
//var words = $words;
Object.keys(words).forEach(x => {
        words[x]['item'] = document.getElementById(x);
    }
);
set_defaults();

console.log(words);




function clicked(x) {
    if (words[x.id]['relations'].length <= 0) { return;}

    //clear();
    //set_defaults();
    //x.style.backgroundColor="red";

    x.classList.add("bg-info-light");
    x.style.textDecoration = "";
    words[x.id]['relations'].forEach(x => words[x]['item'].classList.add("bg-warning"));
}

function hovered(x) {
    if (x.className !== "text-info") {return;}
    x.style.textDecoration = "underline";
}

function unhover(x){
    if (words[x.id]['relations'].length <= 0) { return;}

    set_defaults();
}

function set_defaults() {
    Object.keys(words).forEach(x => {
        var item = words[x]['item'];
        if (words[x]['relations'].length > 0){
            item.className = "text-info";
            item.style.textDecoration = "";
        }
    });
}

function clear() {
    Object.keys(words).forEach(x => {
            words[x]['item'].className = "";
            words[x]['item'].style.textDecoration = "";
        }
    );
}
